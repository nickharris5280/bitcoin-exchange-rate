//
//  FetchExchangeRateData.swift
//  BitcoinExchangeRates
//
//  Created by Nick Harris on 9/22/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

import UIKit

class FetchExchangeRateDataOperation: NSOperation {
    
    let currencyDataUrlString = "https://api.coindesk.com/v1/bpi/currentprice.json"
    var dataController: DataController
    var completion: networkOperationResult
    
    init(dataController: DataController, completion: networkOperationResult) {
        self.dataController = dataController
        self.completion = completion
        super.init()
    }
    
    override func start() {
        
        if let currencyExchangeRateURL = NSURL(string: currencyDataUrlString) {
            
            let session = NSURLSession.sharedSession()
            let request = NSMutableURLRequest(URL: currencyExchangeRateURL)
         
            let task = session.dataTaskWithRequest(request, completionHandler: {
                [unowned self]
                data, response, error -> Void in
                
                if let safeError = error {
                    self.completion(success: false, errorMessage: safeError.localizedDescription)
                    return
                }
                
                guard let safeData = data else {
                    self.completion(success: false, errorMessage: "No data was recieved")
                    return
                }
                
                do {
                    
                    if let json = try NSJSONSerialization.JSONObjectWithData(safeData, options: .MutableLeaves) as? NSDictionary
                    {
                        let bitcoinExchangeRateJSON = try BitcoinCurrentPriceJSON(json: json)
                        
                        NSUserDefaults.standardUserDefaults().setObject(bitcoinExchangeRateJSON.lastUpdate, forKey: DataControllerUserDefaultKeys.lastUpdatedKey.rawValue)
                        
                        for currencyExchangeData in bitcoinExchangeRateJSON.currencies! {
                            ExchangeCurrency.saveCurrencyExchangeData(currencyExchangeData, managedObjectContext: self.dataController.managedObjectContext)
                        }
                        
                        self.dataController.save()
                        self.completion(success: true, errorMessage: nil)
                    }
                    
                    
                }
                catch JSONParseError.MissingRequiredField(let fieldName) {
                    self.completion(success: false, errorMessage: "Response is missing an expected field: \(fieldName)")
                }
                catch let error as NSError {
                    self.completion(success: false, errorMessage: "Parsing the response failed: \(error.localizedDescription)")
                }
                
            })
            
            task.resume()
        }
    }
}
