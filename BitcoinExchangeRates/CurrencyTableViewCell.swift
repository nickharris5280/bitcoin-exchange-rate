//
//  TableViewCell.swift
//  BitcoinExchangeRates
//
//  Created by Nick Harris on 9/22/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

import UIKit

public class CurrencyTableViewCell: UITableViewCell {

    class var ReuseIdentifier: String { return "CurrencyCellReuseID" }
    
    @IBOutlet var currencyNameLabel: UILabel!
    @IBOutlet var currencySymbolLabel: UILabel!
    @IBOutlet var currencyRateLabel: UILabel!

    func configureCell(currency: ExchangeCurrency) {
        
        currencyNameLabel.text = currency.name
        currencySymbolLabel.text = currency.symbol
        currencyRateLabel.text = currency.rate
    }
}
