//
//  ExchangeCurrency.swift
//  BitcoinExchangeRates
//
//  Created by Nick Harris on 9/23/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

import Foundation
import CoreData

@objc(ExchangeCurrency)

class ExchangeCurrency: NSManagedObject {

    class func saveCurrencyExchangeData(currencyExchangeData: CurrencyExchangeData, managedObjectContext: NSManagedObjectContext) {
        
        guard let exchangeCurrency = getExchangeCurrencyObject(currencyExchangeData.code!, managedObjectContext: managedObjectContext) else {
            fatalError("ExchangeCurrency.saveCurrencyExchangeData - NO EXCHANGECURRENCY OBJECT")
        }
        
        exchangeCurrency.code = currencyExchangeData.code
        exchangeCurrency.name = currencyExchangeData.name
        exchangeCurrency.rate = currencyExchangeData.rate
        exchangeCurrency.symbol = currencyExchangeData.symbol
    }

    internal class func getExchangeCurrencyObject(code: String, managedObjectContext: NSManagedObjectContext) -> ExchangeCurrency? {
        
        let fetchRequest = NSFetchRequest(entityName: "ExchangeCurrency")
        fetchRequest.predicate = NSPredicate(format: "code = %@", code)
        
        do {
            guard let fetchResults = try managedObjectContext.executeFetchRequest(fetchRequest) as? [ExchangeCurrency] else {
                fatalError("ExchangeCurrency.getExchangeCurrencyObject - NO FETCH RESULTS")
            }
            
            if fetchResults.count == 1 {
                return fetchResults.last
            }
            else if fetchResults.count == 0 {
                
                guard let exchangeCurrency = NSEntityDescription.insertNewObjectForEntityForName("ExchangeCurrency", inManagedObjectContext: managedObjectContext) as? ExchangeCurrency else {
                    fatalError("ExchangeCurrency.getExchangeCurrencyObject - COULD NOT CREATE EXCHANGECURRENCY OBJECT")
                }
                
                return exchangeCurrency
            }
            else {
                fatalError("ExchangeCurrency.getExchangeCurrencyObject - MULTIPLE FETCH RESULTS FOR CODE")
            }
        }
        catch let error as NSError {
            fatalError("ExchangeCurrency.getExchangeCurrencyObject - ERROR FETCHING FROM CORE DATA: \(error.localizedDescription)")
        }
        
        return nil
    }
}
