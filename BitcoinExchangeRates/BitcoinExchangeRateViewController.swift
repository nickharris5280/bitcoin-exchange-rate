//
//  ViewController.swift
//  BitcoinExchangeRates
//
//  Created by Nick Harris on 9/22/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

import UIKit
import CoreData

class BitcoinExchangeRateViewController: UIViewController, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    var dataController: DataController?
    var fetchedResultsController: NSFetchedResultsController?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        setLastUpdatedLabel()
    }
    
    @IBAction func updateExchangeRateData() {
        fetchExchangeRateData()
    }
    
    func configureWithDataController(dataController: DataController) {
        
        self.dataController = dataController
        
        let currencyFetchRequest = NSFetchRequest(entityName: "ExchangeCurrency")
        let sortDescriptor = NSSortDescriptor(key: "code", ascending: false)
        
        currencyFetchRequest.sortDescriptors = [sortDescriptor]
        
        if let managedObjectContext = self.dataController?.managedObjectContext {
            self.fetchedResultsController = NSFetchedResultsController(
                fetchRequest: currencyFetchRequest,
                managedObjectContext: managedObjectContext,
                sectionNameKeyPath: nil,
                cacheName: nil)
        }
        
        self.fetchedResultsController?.delegate = self
        
        do {
            try self.fetchedResultsController?.performFetch()
        }
        catch let error as NSError {
            fatalError("BitcoinExchangeRateViewController - NSFETCHEDRESULTSCONTROLLER FETCH FAILED: \(error.localizedDescription)")
        }
        
        self.tableView.reloadData()
        fetchExchangeRateData()
    }
    
    internal func fetchExchangeRateData() {
        self.refreshButton.enabled = false
        dataController?.fetchExchangeRateData({
            (success: Bool, errorMessage: String?) in
            
            dispatch_async(dispatch_get_main_queue()) {
                self.refreshButton.enabled = true
                if success {
                    self.setLastUpdatedLabel()
                }
                else {
                    var errorMsg = ""
                    if let safeErrorMessage = errorMessage {
                        errorMsg = safeErrorMessage
                    }
                    
                    let alertController = UIAlertController(title: "Update Failed", message: errorMsg, preferredStyle: .Alert)
                    let cancelAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            }
        })
    }
    
    // MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let currencyCount = fetchedResultsController?.fetchedObjects?.count {
            return currencyCount
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let currencyCell: CurrencyTableViewCell = tableView.dequeueReusableCellWithIdentifier(CurrencyTableViewCell.ReuseIdentifier) as! CurrencyTableViewCell
        
        if let exchangeCurrency = self.fetchedResultsController?.objectAtIndexPath(indexPath) as? ExchangeCurrency {
                currencyCell.configureCell(exchangeCurrency)
        }
        
        return currencyCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 60.0
    }

    // MARK: NSFetchedResultsControllerDelegate
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            guard let newIndexPath = newIndexPath else {
                fatalError("BitcoinExchangeRateViewController - CONTROLLERDIDCHANGEOBJECT CALLED WITH NIL INDEXPATH")
            }
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            
        case .Update:
            guard let indexPath = indexPath else {
                fatalError("BitcoinExchangeRateViewController - CONTROLLERDIDCHANGEOBJECT CALLED WITH NIL INDEXPATH")
            }
            guard let currencyCell = tableView.cellForRowAtIndexPath(indexPath) as? CurrencyTableViewCell else {
                fatalError("BitcoinExchangeRateViewController - COULD NOT GET CELL FOR INDEX PATH")
            }
            guard let exchangeCurrency = anObject as? ExchangeCurrency else {
                fatalError("BitcoinExchangeRateViewController - CONTROLLERDIDCHANGEOBJECT CALLED WITH ANOBJECT NOT AN EXCHANGECURRENCY OBJECT")
            }
            currencyCell.configureCell(exchangeCurrency)
            
        case .Delete:
            break // this tableview does not enable deleting cells
            
        case .Move:
            break // this tableview does not enable moving cells
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
    
    func setLastUpdatedLabel() {
        if let lastUpdatedString = NSUserDefaults.standardUserDefaults().objectForKey(DataControllerUserDefaultKeys.lastUpdatedKey.rawValue) as? String {
            lastUpdatedLabel.text = lastUpdatedString
        }
    }
}

