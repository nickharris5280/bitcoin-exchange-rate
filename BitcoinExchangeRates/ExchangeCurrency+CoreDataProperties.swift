//
//  ExchangeCurrency+CoreDataProperties.swift
//  BitcoinExchangeRates
//
//  Created by Nick Harris on 9/25/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ExchangeCurrency {

    @NSManaged var code: String?
    @NSManaged var name: String?
    @NSManaged var rate: String?
    @NSManaged var symbol: String?

}
