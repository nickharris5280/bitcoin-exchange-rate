//
//  AppDelegate.swift
//  BitcoinExchangeRates
//
//  Created by Nick Harris on 9/22/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var dataController: DataController?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        self.dataController = DataController.init() {
            
            if let bitcoinExchangeRateViewController = self.window?.rootViewController as? BitcoinExchangeRateViewController {
                bitcoinExchangeRateViewController.configureWithDataController(self.dataController!)
            }
        }
        
        return true
    }
}

