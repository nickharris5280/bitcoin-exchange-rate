//
//  DataController.swift
//  BitcoinExchangeRates
//
//  Created by Nick Harris on 9/23/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

import CoreData

typealias networkOperationResult = (success: Bool, errorMessage: String?) -> Void

enum DataControllerUserDefaultKeys: String {
    case lastUpdatedKey = "BitcoinExchangeLastUpdatedKey"
}

class DataController {
    
    var managedObjectContext: NSManagedObjectContext
    internal var privateObjectContext: NSManagedObjectContext
    
    let networkQueue = NSOperationQueue()
    
    init(closure:()->()) {
        
        guard let modelURL = NSBundle.mainBundle().URLForResource("BitcoinExchangeRateDataModel", withExtension: "momd"),
              let managedObjectModel = NSManagedObjectModel.init(contentsOfURL: modelURL)
        else {
            fatalError("DataController - COULD NOT INIT MANAGED OBJECT MODEL")
        }
        
        let coordinator = NSPersistentStoreCoordinator.init(managedObjectModel: managedObjectModel)
            
        self.managedObjectContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.MainQueueConcurrencyType)
        self.privateObjectContext = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
            
        self.privateObjectContext.persistentStoreCoordinator = coordinator
        self.managedObjectContext.parentContext = self.privateObjectContext
        
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            
            let options = [
                NSMigratePersistentStoresAutomaticallyOption: true,
                NSInferMappingModelAutomaticallyOption: true,
                NSSQLitePragmasOption: ["journal_mode": "DELETE"]
            ]
            
            let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).last
            let storeURL = NSURL.init(string: "bitcoinExchangeRate.sqlite", relativeToURL: documentsURL)
            
            do {
                try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: options)
                
                dispatch_async(dispatch_get_main_queue()) {
                    closure()
                }
            }
            catch let error as NSError {
                fatalError("DataController - COULD NOT INIT SQLITE STORE: \(error.localizedDescription)")
            }
        }
    }
    
    func save() {
        
        if self.privateObjectContext.hasChanges || self.managedObjectContext.hasChanges {
            
            self.managedObjectContext.performBlockAndWait {
                
                do {
                    try self.managedObjectContext.save()
                                        
                    self.privateObjectContext.performBlock {
                        
                        do {
                            try self.privateObjectContext.save()
                        }
                        catch let error as NSError {
                            fatalError("DataController - SAVE PRIVATEOBJECTCONTEXT FAILED: \(error.localizedDescription)")
                        }
                    }
                }
                catch let error as NSError {
                    fatalError("DataController - SAVE MANAGEDOBJECTCONTEXT FAILED: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func fetchExchangeRateData(completion: networkOperationResult) {
        
        let fetchExchangeRateDataOperation = FetchExchangeRateDataOperation(dataController: self, completion: completion)
        self.networkQueue.addOperation(fetchExchangeRateDataOperation)
    }
}
