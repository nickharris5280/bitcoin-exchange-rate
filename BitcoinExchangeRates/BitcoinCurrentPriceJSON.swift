//
//  BitcoinCurrentPriceJSON.swift
//  BitcoinExchangeRates
//
//  Created by Nick Harris on 9/25/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

import UIKit

enum JSONParseError: ErrorType {
    case MissingRequiredField(fieldName: String)
}

struct CurrencyExchangeData {
    var code: String?
    var name: String?
    var rate: String?
    var symbol: String?
}

struct BitcoinCurrentPriceJSON {
    
    enum BitcoinCurrentPriceJSONKeys: String {
        case time = "time"
        case updated = "updated"
        case bpi = "bpi"
        case code = "code"
        case description = "description"
        case rate = "rate"
        case symbol = "symbol"
    }
    
    var lastUpdate: String?
    var currencies: [CurrencyExchangeData]?
    
    init(json: NSDictionary) throws {
        self.currencies = []
        try parseExchangeRateJSON(json)
    }
    
    internal mutating func parseExchangeRateJSON(json: NSDictionary) throws -> BitcoinCurrentPriceJSON {
        
        guard let timeDictionary = json[BitcoinCurrentPriceJSONKeys.time.rawValue] as? NSDictionary,
            let timeStamp = timeDictionary[BitcoinCurrentPriceJSONKeys.updated.rawValue] as? String else
        {
            throw JSONParseError.MissingRequiredField(fieldName: BitcoinCurrentPriceJSONKeys.updated.rawValue)
        }
        
        self.lastUpdate = timeStamp
        
        guard let bpiDictionary = json[BitcoinCurrentPriceJSONKeys.bpi.rawValue] as? NSDictionary,
            let currencies = bpiDictionary.allValues as? [NSDictionary] else
        {
            throw JSONParseError.MissingRequiredField(fieldName: BitcoinCurrentPriceJSONKeys.bpi.rawValue)
        }
        
        for currencyDictionary in currencies {
            let currencyExchangeData = try parseCurrencyDictionary(currencyDictionary as NSDictionary)
            self.currencies?.append(currencyExchangeData)
        }
        
        return self
    }
    
    internal func parseCurrencyDictionary(currencyDictionary: NSDictionary) throws -> CurrencyExchangeData {
        
        guard let currencyCode = currencyDictionary[BitcoinCurrentPriceJSONKeys.code.rawValue] as? String else
        {
            throw JSONParseError.MissingRequiredField(fieldName: BitcoinCurrentPriceJSONKeys.code.rawValue)
        }
        
        guard let currencyDescription = currencyDictionary[BitcoinCurrentPriceJSONKeys.description.rawValue] as? String else
        {
            throw JSONParseError.MissingRequiredField(fieldName: BitcoinCurrentPriceJSONKeys.description.rawValue)
        }
        
        guard let currencyRate = currencyDictionary[BitcoinCurrentPriceJSONKeys.rate.rawValue] as? String else
        {
            throw JSONParseError.MissingRequiredField(fieldName: BitcoinCurrentPriceJSONKeys.rate.rawValue)
        }
        
        guard let currencySymbol = currencyDictionary[BitcoinCurrentPriceJSONKeys.symbol.rawValue] as? String else
        {
            throw JSONParseError.MissingRequiredField(fieldName: BitcoinCurrentPriceJSONKeys.symbol.rawValue)
        }
        
        var currencyExchangeData = CurrencyExchangeData()
        currencyExchangeData.code = currencyCode
        currencyExchangeData.name = currencyDescription
        currencyExchangeData.rate = currencyRate
        currencyExchangeData.symbol = decodeSymbol(currencySymbol)
        
        return currencyExchangeData
    }
    
    internal func decodeSymbol(encodedSymbolString: String) -> String {
        let encodedData = encodedSymbolString.dataUsingEncoding(NSUTF8StringEncoding)!
        let attributedOptions : [String: AnyObject] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding
        ]
        
        do {
            let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
            return attributedString.string
            
        }
        catch {
            return ""
        }
    }
}